import React, {useEffect, useState} from 'react'

function ConferenceForm(props){
    const[locations,setLocations]= useState([])

    const [name, setName] =useState('')
    const handleNameChange=(event) => {
        const value =event.target.value;
        setName(value);
    }

    const[startDate, setStartDate] = useState('');
    const handleStartDayChange =(event) => {
        const value =event.target.value;
        setStartDate(value);
    }

    const{endDate, setEndDate} =useState('');
    const handleEndDateChange =(event) =>{
        const value =event.target.value;
        setEndDate(value);
    }

    const [description, setDescription] = useState('');
    const handleDescriptionChange = (event) => {
        const value = event.target.value;
        setDescription(value);
    }

    const [maxPres, setMaxPres] = useState('');
    const handleMaxPresChange = (event) => {
        const value = event.target.value;
        setMaxPres(value);
    }

    const [maxAttend, setMaxAttend] = useState('');
    const handleMaxAttendChange = (event) => {
        const value = event.target.value;
        setMaxAttend(value);
    }

    const [location, setLocation] = useState('');
    const handleLocationChange = (event) => {
        const value = event.target.value;
        setLocation(value);
    }

    const fetchData = async() =>{
        const url = 'http://localhost:8000/api/locations/';

        const response = await fetch(url);

        if (response.ok){
            const data = await response.json()
            setLocations(data.locations)

        }
    }



    useEffect(()=>{
        fetchData();
    },[]);


    const handleSubmit = async (event) => {
        event.preventDefault();

        const data = {}
        data.name = name;
        data.starts = startDate;
        data.ends = endDate;
        data.description = description;
        data.max_presentations = maxPres;
        data.max_attendees = maxAttend;
        data.location = location;



        const locationUrl = 'http://localhost:8000/api/conferences/';
        const fetchConfig = {
            method: 'POST',
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };

        const response = await fetch(locationUrl, fetchConfig);
        if(response.ok){
            const newConference = await response.json();

            setName('');
            setStartDate('');
            setEndDate('');
            setDescription('');
            setMaxPres('');
            setMaxAttend('');
        }
    }


    return(
    <div className="row">
    <div className="offset-3 col-6">
      <div className="shadow p-4 mt-4">
        <h1>Create a new Conference</h1>
        <form id="create-conference-form">
          <div className="form-floating mb-3">
            <input onChange={handleNameChange} placeholder="Name" required type="text" name="name" id="name" className="form-control"/>
            <label htmlFor="name">Name</label>
          </div>
          <div className="form-floating mb-3">
            <input onChange={handleStartDayChange} placeholder="Starts" required type="date" name="starts" id="starts" className="form-control"/>
            <label htmlFor="starts">Start Date</label>
          </div>
          <div className="form-floating mb-3">
            <input onChange={handleEndDateChange} placeholder="Ends" required type="date" name="ends" id="ends" className="form-control"/>
            <label htmlFor="city">End Date</label>
          </div>
          <div className="mb-3">
            <label htmlFor="description" className = "form-label">Description</label>
            <textarea onChange={handleDescriptionChange} className="form-control" name= "description" id="description" rows="3"></textarea>
          </div>
          <div className="form-floating mb-3">
            <input onChange={handleMaxPresChange} placeholder="Maximum Presentations" required type="number" name="max_presentations" id="max_presentations" className="form-control"/>
            <label htmlFor="max_presentations">Maximum Presentations</label>
            <div className="form-floating mb-3">
                <input onChange={handleMaxAttendChange} placeholder="Maximum attendees" required type="number" name="max_attendees" id="max_attendees" className="form-control"/>
                <label htmlFor="max_attendees">Maximum attendees</label>
            </div>
         <div className="mb-3">
            <select onChange={handleLocationChange} required id="location" name="location" className="form-select">
              <option value="">Choose a location</option>
              {locations.map(location =>{
                return(
                    <option key={location.id} value = {location.id}>
                        {location.name}
                    </option>
                )})}
            </select>
          </div>
          <button className="btn btn-primary">Create</button>
        </div>
        </form>
      </div>
    </div>
  </div>
);
}

export default ConferenceForm