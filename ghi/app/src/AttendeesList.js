import Nav from './Nav'


function AttendeesList(props){
    return (
        <table className = "table table-striped">
        <thead>
          <tr>
            <th style = {{ width: "60%"}}>Name</th>
            <th style = {{ width: "60%"}}>Conference</th>
          </tr>
        </thead>
        <tbody>

          {props .attendees && props.attendees.map(attendee => {
            return(
              <tr key={attendee.href}>
                <td>{attendee.name}</td>
                <td>{attendee.conference}</td>
              </tr>
            );
          })}
        </tbody>
      </table>
    );

}

export default AttendeesList;